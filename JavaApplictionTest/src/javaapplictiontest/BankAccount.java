/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplictiontest;

import java.sql.Time;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author afarkas
 */
class BankAccount  implements Subject,Observer{
    private final long id;
    private String user;
    private double balance;
    private double amount;
    private long transferId;
    private final ArrayList<Observer> observers;
    private TransactionTypes currentTransaction;
    private final TransactionHistory transactionHis;
    
    public BankAccount(String user,double balance){
        this.id = Instant.now().getEpochSecond();
        this.balance = balance;
        observers = new ArrayList<>();
        transactionHis = new TransactionHistory(this);
        observers.add(this);
        this.user = user;
    }

    public void showTransHistory(TransactionTypes type,Date filterDate) {
        transactionHis.showHistory(currentTransaction, filterDate);
    }

    public double getBalance() {
        return balance;
    }

    public long getId() {
        return id;
    }
   
     
     public void withdraw(double amount){
        this.amount = amount;
        if((balance - amount) > 0){
            balance -= amount;
            this.currentTransaction = TransactionTypes.withdraw;
            statusChanged();
        }
        else{
            System.out.println("You cannot withdraw!");
            System.out.println("");
        }
    }

    public void deposit(double amount){
        this.amount = amount;
        balance += amount;
        this.currentTransaction = TransactionTypes.deposit;
        statusChanged();
    }
    
    public void transfer(double amount,long bankaccountId)
    {
        this.amount = amount;
        this.balance -= amount;
        transferId = bankaccountId;
        statusChanged();
    }

     public void statusChanged(){
        notifyObservers();
    }
    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
            observers.remove(o);
    
    }

    @Override
    public void notifyObservers() {
        observers.stream().forEach((o) -> {
            if (transferId > 0)
            {
                o.update(new Transaction(amount,currentTransaction,balance,transferId));
            }else
            {
                o.update(new Transaction(amount,currentTransaction,balance));
            }
        });
    }

    @Override
    public void update(Transaction transaction) {
       if (transaction.getBankAccountid() == id)
       {
           balance += transaction.getAmount();
           System.out.println("Account with "+ id +" id received "+transaction.getAmount()+" amount of money ");
       }
    }
}
