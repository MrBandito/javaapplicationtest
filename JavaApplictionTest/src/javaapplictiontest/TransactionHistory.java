    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplictiontest;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author afarkas
 */
class TransactionHistory implements Observer {
   private final BankAccount bankAccount;
   private final List<Transaction> transactions;

    public TransactionHistory(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
        bankAccount.registerObserver(this);
        transactions = new ArrayList<>();
    }
    public void showHistory(TransactionTypes type,Date filterDate)
    {
           transactions.stream().filter((transaction) -> 
                   ((filterDate !=null && transaction.getDate().compareTo(filterDate)<=0 )|| transaction.getType() == type)).forEach((transaction) -> {
            System.out.println(String.format("Date : %s, Amount %f, type : %s, Current Balance %f",
                    transaction.getDate().toString(),transaction.getAmount(),transaction.getType().toString(),transaction.getCurrentBalance()));
       }); 
     
        
    }
    @Override
    public void update(Transaction transaction) {
        transactions.add(transaction);
    }
    
}
