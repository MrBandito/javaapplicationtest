/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplictiontest;

import java.time.Instant;
import java.util.Date;

/**
 *
 * @author afarkas
 */
public class Transaction {
    private final Date date;
    private final double amount;
    private final TransactionTypes type;
    private final double currentBalance ;
    private final long bankAccountid;

    public long getBankAccountid() {
        return bankAccountid;
    }
    
    public TransactionTypes getType() {
        return type;
    }

    public Date getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }

     public double getCurrentBalance() {
        return currentBalance;
    }
     
    public Transaction(double amount, TransactionTypes type,double currentBalance) {
        this.date = Date.from(Instant.now());
        this.amount = amount;
        this.type = type;
        this.currentBalance = currentBalance;
        this.bankAccountid = 0;
    }
public Transaction(double amount, TransactionTypes type,double currentBalance,long  accountID) {
        this.date = Date.from(Instant.now());
        this.amount = amount;
        this.type = type;
        this.currentBalance = currentBalance;
        this.bankAccountid = accountID;
    }
    
    
    
}

