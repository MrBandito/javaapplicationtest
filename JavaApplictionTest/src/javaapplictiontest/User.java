/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplictiontest;

/**
 *
 * @author afarkas
 */
public class User {
    private long id;
    private String name;
    private int budget;
    private BankAccount bankAccount;

    public User(long id, String name, int budget, BankAccount bankAccount) {
        this.id = id;
        this.name = name;
        this.budget = budget;
        this.bankAccount = bankAccount;
    }
}
